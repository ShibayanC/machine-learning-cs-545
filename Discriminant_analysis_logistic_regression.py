import numpy as np
import pandas as pd
import scaledconjugategradient as scg
import matplotlib.pyplot as plt
import random
import multiprocessing as mp
import mlutils as ml
%matplotlib inline

class NeuralNetwork:
    def __init__(self,ni,nhs,no):
        try:
            nihs = [ni] + list(nhs)
        except:
            nihs = [ni] + [nhs]
            nhs = [nhs]
        self.Vs = [np.random.uniform(-0.1,0.1,size=(1+nihs[i],nihs[i+1])) for i in range(len(nihs)-1)]
        self.W = np.random.uniform(-0.1,0.1,size=(1+nhs[-1],no))
        self.ni,self.nhs,self.no = ni,nhs,no
        self.Xmeans = None
        self.Xstds = None
        self.Tmeans = None
        self.Tstds = None
        self.iteration = mp.Value('i',0)
        self.trained = mp.Value('b',False)
        self.reason = None
        self.errorTrace = None
        
    def getSize(self):
        return (self.ni,self.nhs,self.no)

    def getErrorTrace(self):
        return self.errorTrace
    
    def getNumberOfIterations(self):
        return self.numberOfIterations
        
    def train(self,X,T,
              nIterations=100,weightPrecision=0,errorPrecision=0,verbose=False):
        if self.Xmeans is None:
            self.Xmeans = X.mean(axis=0)
            self.Xstds = X.std(axis=0)
        X = self._standardizeX(X)

        if T.ndim == 1:
            T = T.reshape((-1,1))

        if self.Tmeans is None:
            self.Tmeans = T.mean(axis=0)
            self.Tstds = T.std(axis=0)
        T = self._standardizeT(T)

        # Local functions used by gradientDescent.scg()

        def objectiveF(w):
            self._unpack(w)
            Y,_ = self._forward_pass(X)
            return 0.5 * np.mean((Y - T)**2)

        def gradF(w):
            self._unpack(w)
            Y,Z = self._forward_pass(X)
            delta = (Y - T) / (X.shape[0] * T.shape[1])
            dVs,dW = self._backward_pass(delta,Z)
            return self._pack(dVs,dW)

        scgresult = scg.scg(self._pack(self.Vs,self.W), objectiveF, gradF,
                            xPrecision = weightPrecision,
                            fPrecision = errorPrecision,
                            nIterations = nIterations,
                            iterationVariable = self.iteration,
                            ftracep=True,
                            verbose=verbose)

        self._unpack(scgresult['x'])
        self.reason = scgresult['reason']
        self.errorTrace = scgresult['ftrace']
        self.numberOfIterations = len(self.errorTrace) - 1
        self.trained.value = True
        return self
    
    def use(self,X,allOutputs=False):
        Xst = self._standardizeX(X)
        Y,Z = self._forward_pass(Xst)
        Y = self._unstandardizeT(Y)
        return (Y,Z[1:]) if allOutputs else Y

    def draw(self,inputNames = None, outputNames = None):
        ml.draw(self.Vs, self.W, inputNames, outputNames)

    def __repr__(self):
        str = 'NeuralNetwork({}, {}, {})'.format(self.ni,self.nhs,self.no)
        if self.trained:
            str += '\n   Network was trained for {} iterations. Final error is {}.'.format(self.numberOfIterations,
                                                                                           self.errorTrace[-1])
        else:
            str += '  Network is not trained.'
        return str
            
    def _standardizeX(self,X):
        return (X - self.Xmeans) / self.Xstds
    def _unstandardizeX(self,Xs):
        return self.Xstds * Xs + self.Xmeans
    def _standardizeT(self,T):
        return (T - self.Tmeans) / self.Tstds
    def _unstandardizeT(self,Ts):
        return self.Tstds * Ts + self.Tmeans

    def _forward_pass(self,X):
        Zprev = X
        Zs = [Zprev]
        Y = np.dot(Zprev, self.W[1:,:]) + self.W[0:1,:]
        return Y, Zs

    def _backward_pass(self,delta,Z):
        dW = np.vstack((np.dot(np.ones((1,delta.shape[0])),delta),  np.dot( Z[-1].T, delta)))
        dVs = []        
        return dVs,dW

    def _pack(self,Vs,W):
        return np.hstack([W.flat])

    def _unpack(self,w):
        first = 0
        numInThisLayer = self.ni        
        self.W[:] = w[first:].reshape((numInThisLayer+1,self.no))

    def pickleDump(self,filename):
        n = self.iteration.value
        t = self.trained.value
        self.iteration = None
        self.trained = None
        with open(filename,'wb') as fp:
        #    pickle.dump(self,fp)
            cPickle.dump(self,fp)
        self.iteration = mp.Value('i',n)
        self.trained = mp.Value('b',t)

def makeIndicatorVars(T):
    # Make sure T is two-dimensiona. Should be nSamples x 1.
    if T.ndim == 1:
        T = T.reshape((-1,1))    
    return (T == np.unique(T)).astype(int)

def g(X,beta):
    fs = np.exp(np.dot(X, beta))  # N x K-1
    denom = (1 + np.sum(fs,axis=1)).reshape((-1,1))
    gs = fs / denom
    return np.hstack((gs,1/denom))

def percentCorrect(p,t):
    return np.sum(p.ravel()==t.ravel()) / float(len(t)) * 100


class NeuralNetworkClassifier(NeuralNetwork):
    def __init__(self,ni,nhs,no):
        NeuralNetwork.__init__(self,ni,nhs,no-1)

    def _multinomialize(self,Y):
        mx = np.max(Y)
        expY = np.exp(Y-mx)
        denom = np.exp(-mx) + np.sum(expY,axis=1).reshape((-1,1))
        rowsHavingZeroDenom = denom == 0.0
        if np.sum(rowsHavingZeroDenom) > 0:
            Yshape = (expY.shape[0],expY.shape[1]+1)
            nClasses = Yshape[1]
            Y = np.ones(Yshape) * 1.0/nClasses + np.random.uniform(0,0.1,Yshape)
            Y /= np.sum(Y,1).reshape((-1,1))
        else:
            Y = np.hstack((expY / denom, np.exp(-mx)/denom))
        return Y

    def train(self,X,T,
                 nIterations=100,weightPrecision=0,errorPrecision=0,verbose=False):
        if self.Xmeans is None:
            self.Xmeans = X.mean(axis=0)
            self.Xstds = X.std(axis=0)
        X = self._standardizeX(X)

        self.classes = np.unique(T)
        if self.no != len(self.classes)-1:
            raise ValueError(" In NeuralNetworkClassifier, the number of outputs must be one less than\n the number of classes in the training data. The given number of outputs\n is %d and number of classes is %d. Try changing the number of outputs in the\n call to NeuralNetworkClassifier()." % (self.no, len(self.classes)))
        T = ml.makeIndicatorVars(T)

        def objectiveF(w):
            self._unpack(w)
            Y,_ = self._forward_pass(X)
            Y = self._multinomialize(Y)
            return -np.mean(T * np.log(Y))

        def gradF(w):
            self._unpack(w)
            Y,Z = self._forward_pass(X)
            Y = self._multinomialize(Y)
            delta = (Y[:,:-1] - T[:,:-1]) / (X.shape[0] * (T.shape[1]-1))
            dVs,dW = self._backward_pass(delta,Z)
            return self._pack(dVs,dW)

        scgresult = scg.scg(self._pack(self.Vs,self.W), objectiveF, gradF,
                            xPrecision = weightPrecision,
                            fPrecision = errorPrecision,
                            nIterations = nIterations,
                            iterationVariable = self.iteration,
                            ftracep=True,
                            verbose=verbose)

        self._unpack(scgresult['x'])
        self.reason = scgresult['reason']
        self.errorTrace = scgresult['ftrace']
        self.numberOfIterations = len(self.errorTrace) - 1
        self.trained.value = True
        return self
    
    def use(self,X,allOutputs=False):
        Xst = self._standardizeX(X)
        Y,Z = self._forward_pass(Xst)
        Y = self._multinomialize(Y)
        classes = self.classes[np.argmax(Y,axis=1)].reshape((-1,1))
        return (classes,Y,Z[1:]) if allOutputs else classes

# Analysis using linear logistic regression
trainF = 0.8

p1I,_ = np.where(T == 0)
p2I,_ = np.where(T == 1)
p3I,_ = np.where(T == 2)

p1I = np.random.permutation(p1I)
p2I = np.random.permutation(p2I)
p3I = np.random.permutation(p3I)

np1 = round(trainF * len(p1I))
np2 = round(trainF * len(p2I))
np3 = round(trainF * len(p3I))

rowsTrain = np.hstack((p1I[:np1], p2I[:np2], p3I[:np3]))
Xtrain = X[rowsTrain,:]
Ttrain = T[rowsTrain,:]
rowsTest = np.hstack((p1I[np1:], p2I[np2:], p3I[np3:]))
Xtest = X[rowsTest,:]
Ttest = T[rowsTest,:]

standardize,_ = makeStandardize(Xtrain)
Xtrains = standardize(Xtrain)
Xtests = standardize(Xtest)

Xtrains.shape, Xtests.shape

Xtrains1 = np.hstack(( np.ones((Xtrains.shape[0],1)), Xtrains))
Xtests1 = np.hstack(( np.ones((Xtests.shape[0],1)), Xtests))

Xtrains1.shape, Xtests1.shape

TtrainI = makeIndicatorVars(Ttrain)
TtestI = makeIndicatorVars(Ttest)

TtrainI.shape, TtestI.shape

beta = np.zeros((Xtrains1.shape[1],TtrainI.shape[1]-1))
alpha = 0.0001
for step in range(1000):
    gs = g(Xtrains1,beta)
    beta = beta + alpha * np.dot(Xtrains1.T, TtrainI[:,:-1] - gs[:,:-1])
    likelihoodPerSample = np.exp( np.sum(TtrainI * np.log(gs)) / Xtrains.shape[0])
    # print("Step",step," l =",likelihoodPerSample)

logregOutput = g(Xtrains1,beta)
predictedTrain = np.argmax(logregOutput,axis=1)
logregOutput.shape

#print (Xtest.shape)
order = np.argsort(Xtrain[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtrain[order], logregOutput[order]);
plt.xlabel('Xtrain');
plt.ylabel('Prob distribution');
plt.legend(('$P(Class 1)$','$P(Class 2)$', '$P(Class 3)$'));
plt.savefig('e520')

logregOutput = g(Xtests1,beta)
predictedTest = np.argmax(logregOutput,axis=1)
logregOutput.shape

#print (Xtest.shape)
order = np.argsort(Xtest[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtest[order], logregOutput[order]);
plt.xlabel('Xtest');
plt.ylabel('Prob distribution');
plt.legend(('$P(Class 1)$','$P(Class 2)$', '$P(Class 3)$'));
plt.savefig('e521')

print("LogReg: Percent correct: Train {:.3g} Test {:.3g}".format(percentCorrect(predictedTrain,Ttrain),percentCorrect(predictedTest,Ttest)))

plt.figure(figsize = (8, 6))
order = np.argsort(Xtrain[:,0])
plt.plot(Xtrain[order], predictedTrain[order], '-', alpha = 0.98)
plt.plot(Xtrain[order], Ttrain[order], 'o', alpha = 0.45)
plt.ylim(-0.3, 2.2)
plt.xlabel('Xtrain');
plt.ylabel('Target Class');
plt.legend(('Predicted Train','Actual Train',),loc = 'best');
plt.savefig('e522')

plt.figure(figsize = (8, 6))
order = np.argsort(Xtest[:,0])
plt.plot(Xtest[order], predictedTest[order], '-', alpha = 0.98)
plt.plot(Xtest[order], Ttest[order], 'o', alpha = 0.45)
plt.ylim(-0.2, 2.2)
plt.xlabel('Xtest');
plt.ylabel('Target Class');
plt.legend(('Predicted Test','Actual Test',),loc = 'best');
plt.savefig('e523')

Xtrain.shape, Xtest.shape, Ttrain.shape, Ttest.shape

nHiddenUnits = 1
nnet = NeuralNetworkClassifier(Xtrain.shape[1],nHiddenUnits,3)
nnet.train(Xtrain,Ttrain,nIterations = 100)
#nNew = 100
#newData = np.linspace(0.0,4.0,nNew).repeat(D).reshape((nNew,D))
predictedClass,predictedProbabilities,Z = nnet.use(Xtest,allOutputs=True)

plt.figure(figsize=(8,6))
plt.plot(Xtrain,Ttrain,'o')
plt.ylabel("Target Classes")
plt.xlabel("xs")
plt.legend(('Data',),loc = 'best')
plt.ylim(-0.2,2.2)

plt.figure(figsize=(8,6))
order = np.argsort(Xtest[:,0])
plt.plot(Xtest[order],predictedProbabilities[order])
plt.ylabel(" LLR P(Class=k|x) for k = 1,2 and 3", multialignment="center");
plt.legend(('P(C|k =1)','P(C|k =2)','P(C|k =3)'), loc = 'best')
plt.xlabel('Xtest')
plt.savefig('d520')

class1 = predictedClass==0
class2 = predictedClass==1
class3 = predictedClass==2

Xtest.shape, Ttest.shape, predictedClass.shape

plt.figure(figsize=(8,6))
order = np.argsort(Xtest[:,0])
plt.plot(Xtest[order], predictedClass[order], 'o-', alpha = 0.99)
order = np.argsort(Xtrain[:,0])
plt.plot(Xtrain[order], Ttrain[order], 'o', alpha = 0.55)
plt.xlabel("Xtest")
plt.ylabel("Predicted Test")
plt.ylim(-0.2,2.2)
plt.legend(('predicted Class','Ttrain'),loc='best');

plt.figure(figsize = (8,6))
order = np.argsort(Xtrain[:,0])
plt.plot(Xtrain[order], predictedClass, 'o-', alpha = 0.98)
plt.plot(Xtrain[order], Ttrain, 'o', alpha = 0.55)
plt.xlabel("Xtrain")
plt.ylabel("Target Class")
plt.ylim(-0.2,2.2)
plt.legend(('predictedClass','Actual Train'),loc='best');
plt.savefig('a513')

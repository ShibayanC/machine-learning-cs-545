import numpy as np
import random
import pandas as pd
import math
import matplotlib.pyplot as plt
%matplotlib inline

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

%precision 2
dataSet = pd.read_csv(open('iris.data'), header = None)
data = dataSet.values
data = data[0:150]
data.shape

def makeStandardize(X):
    means = X.mean(axis=0)
    stds = X.std(axis=0)
    def standardize(origX):
        return (origX - means) / stds
    def unStandardize(stdX):
        return stds * stdX + means
    return (standardize, unStandardize)

D = 1

a = data[:,2:3]
b = data[:,3:4]
t1 = [0]*50
t2 = [1]*50
t3 = [2]*50

ts = np.vstack((np.hstack((t1, t2, t3))))
a.shape, b.shape, ts.shape

f = open("iris.data","r")
data = np.loadtxt(f ,delimiter=',', usecols=np.arange(4))
xs = data[:,2:3]
standardize,_ = makeStandardize(xs)

xs = standardize(xs)
xs.mean(0), xs.std(0)

t1 = [0]*50
t2 = [1]*50
t3 = [2]*50

ts = np.vstack((np.hstack((t1, t2, t3))))
xs.shape, ts.shape

plt.figure(figsize = (8,6))
colors = ["red", "blue", "green"]
plt.plot(xs, ts, 'o', alpha = 0.65);
plt.ylim(-0.2, 2.2);
plt.xlabel('Petal-Length');
plt.ylabel('Class');
plt.savefig('d506')

def discQDA(X, standardize, mu, Sigma, prior):
    Xc = standardize(X) - mu
    if Sigma.size == 1:
        Sigma = np.asarray(Sigma).reshape((1,1))
    det = np.linalg.det(Sigma)        
    if det == 0:
        raise np.linalg.LinAlgError('discQDA(): Singular covariance matrix')
    SigmaInv = np.linalg.inv(Sigma)     # pinv in case Sigma is singular
    return -0.5 * np.log(det) \
           - 0.5 * np.sum(np.dot(Xc,SigmaInv) * Xc, axis=1) \
           + np.log(prior)

trainF = 0.8
p1I,_ = np.where(T == 0)
p2I,_ = np.where(T == 1)
p3I,_ = np.where(T == 2)

p1I = np.random.permutation(p1I)
p2I = np.random.permutation(p2I)
p3I = np.random.permutation(p3I)

np1 = round(trainF * len(p1I))
np2 = round(trainF * len(p2I))
np3 = round(trainF * len(p3I))

rowsTrain = np.hstack((p1I[:np1], p2I[:np2], p3I[:np3]))
Xtrain = X[rowsTrain,:]
Ttrain = T[rowsTrain,:]
rowsTest = np.hstack((p1I[np1:], p2I[np2:], p3I[np3:]))
Xtest = X[rowsTest,:]
Ttest = T[rowsTest,:]

Xtrain.shape, Ttrain.shape, Xtest.shape, Ttest.shape

standardize,_ = makeStandardize(Xtrain)
Xtrains = standardize(Xtrain)
Xtests = standardize(Xtest)

Ttr = (Ttrain == 0).reshape((-1))
mu1 = np.mean(Xtrains[Ttr,:], axis = 0)
cov1 = np.cov(Xtrains[Ttr,:].T)
Ttr = (Ttrain.ravel() == 1).reshape((-1))
mu2 = np.mean(Xtrains[Ttr,:], axis = 0)
cov2 = np.cov(Xtrains[Ttr,:].T)
Ttr = (Ttrain.ravel() == 2).reshape((-1))
mu3 = np.mean(Xtrains[Ttr,:], axis = 0)
cov3 = np.cov(Xtrains[Ttr,:].T)

np1, np2, np3

d1 = discQDA(Xtrains, standardize, mu1, cov1, float(np1)/(np1 + np2 + np3))
d2 = discQDA(Xtrains, standardize, mu2, cov2, float(np2)/(np1 + np2 + np3))
d3 = discQDA(Xtrains, standardize, mu3, cov3, float(np3)/(np1 + np2 + np3))
predictedTrain = np.argmax(np.vstack((d1, d2, d3)), axis = 0)

d1.shape, d2.shape, d3.shape, predictedTrain.shape

d1t = discQDA(Xtests, standardize, mu1, cov1, float(np1)/(np1 + np2 + np3))
d2t = discQDA(Xtests, standardize, mu2, cov2, float(np2)/(np1 + np2 + np3))
d3t = discQDA(Xtests, standardize, mu3, cov3, float(np3)/(np1 + np2 + np3))
predictedTest = np.argmax(np.vstack((d1t, d2t, d3t)), axis = 0)

d1t.shape, d2t.shape, d3t.shape, predictedTest.shape

def percentCorrect(p, t):
    return np.sum(p.ravel()==t.ravel()) / float(len(t)) * 100

print ('Percent correct: Ttrain', percentCorrect(predictedTrain, Ttrain),'Ttest',percentCorrect(predictedTest, Ttest))

plt.figure(figsize = (8,5))
xs = Xtrain[:,0:1]
order = np.argsort(xs[:,0])
y = np.vstack((d1, d2, d3)).T
plt.plot(xs[order], y[order]);
plt.ylabel("QDA Discriminant Functions")
plt.xlabel('Xvalues')
plt.legend(('d1','d2','d3'), loc = 'best')
plt.savefig('d515')

a = np.log(np.array([[np1,np2,np3]]))
b = np.vstack((d1,d2,d3)).T
# Plot generative distributions  p(x | Class=k)  starting with discriminant functions
probs = np.exp( np.vstack((d1,d2,d3)).T - 0.5*D*np.log(2*np.pi) - np.log(np.array([[np1,np2,np3]])))
probs.shape

order = np.argsort(Xtrain[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtrain[order],probs[order],'-',alpha = 0.8);
plt.ylabel("QDA P(x|Class=k)\n from disc funcs", multialignment="center")
plt.xlabel('Xtest')
plt.legend(('$P(Class 1)$','$P(Class 2)$','$P(Class 3)$'))
plt.savefig('d516')

# Plot generative distributions  p(x | Class=k)  using normald
XS = standardize(Xtrain)
probs = np.hstack((normald(XS,mu1,cov1), normald(XS,mu2,cov2), normald(XS,mu3,cov3)))
order = np.argsort(Xtrain[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtrain[order],probs[order],'-',alpha = 0.8);
plt.ylabel("QDA P(x|Class=k)\n using normald", multialignment="center");
plt.xlabel('Xtest')
plt.legend(('$P(Class 1)$','$P(Class 2)$','$P(Class 3)$'))
plt.savefig('d517')

plt.figure(figsize = (8,6))
order = np.argsort(Xtrain[:,0])
plt.plot(Xtrain[order], predictedTrain[order], '-', alpha = 0.98)
plt.plot(Xtrain, Ttrain, 'o', alpha = 0.65)
plt.ylim(-0.2, 2.2)
plt.xlabel('X Values')
plt.ylabel('Target Class')
plt.legend(('Predicted Train', 'Actual Train'), loc = 'best')
plt.savefig('d518')

plt.figure(figsize = (8,6))
order = np.argsort(Xtest[:,0])
plt.plot(Xtest[order], predictedTest[order], '-', alpha = 0.98)
plt.plot(Xtest, Ttest, 'o', alpha = 0.65)
plt.ylim(-0.2, 2.2)
plt.xlabel('X Values')
plt.ylabel('Target Class')
plt.legend(('Predicted Test', 'Actual Test'), loc = 'best')
plt.savefig('d519')

import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
%matplotlib inline

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

!head iris.data

%precision 2
dataSet = pd.read_csv(open('iris.data'), header = None)
data = dataSet.values
data = data[0:150]
data.shape

def makeStandardize(X):
    means = X.mean(axis=0)
    stds = X.std(axis=0)
    def standardize(origX):
        return (origX - means) / stds
    def unStandardize(stdX):
        return stds * stdX + means
    return (standardize, unStandardize)

f = open("iris.data","r") # Opening the dataset 'iris.data' 
data = np.loadtxt(f ,delimiter=',', usecols=np.arange(4))
xs = data[:,2:3] # Selecting the petal-length attribute
standardize,_ = makeStandardize(xs)

xs = standardize(xs) # Standardizing the input values
xs.mean(0), xs.std(0) # Calculating mean and standard deviation of the dataset

D = 1 # No of sample in each row
# Defining Class 1,2,3
t1 = [0]*50
t2 = [1]*50
t3 = [2]*50
# Appending the values of target variables 
ts = np.vstack((np.hstack((t1, t2, t3))))
xs.shape, ts.shape

Xdata = np.vstack((np.hstack((xs,ts))))
print ('Size of DataSet: ',Xdata.shape)
print (len(Xdata))
    
names = ['Petal-length','Type']
print (Xdata[0:5])

def normald(X, mu=None, sigma=None):
    d = X.shape[1]
    if np.any(mu == None):
        mu = np.zeros((d,1))
    if np.any(sigma == None):
        sigma = np.eye(d)
    detSigma = sigma if d == 1 else np.linalg.det(sigma)
    if detSigma == 0:
        raise np.linalg.LinAlgError('normald(): Singular covariance matrix')
    sigmaI = 1.0/sigma if d == 1 else np.linalg.inv(sigma)
    normConstant = 1.0 / np.sqrt((2*np.pi)**d * detSigma)
    diffv = X - mu.T # change column vector mu to be row vector
    return normConstant * np.exp(-0.5 * np.sum(np.dot(diffv, sigmaI) * diffv, axis=1))[:,np.newaxis]

def addOnes(A):
    return np.hstack((np.ones((A.shape[0],1)), A))

X = Xdata[:,0:1]
T = Xdata[:,1:2]
print ('X.shape & T.shape: ',X.shape, T.shape)

cl1Rows = T == 0 # Selecting class 1
cl2Rows = T == 1 # Selecting class 2
cl3Rows = T == 2 # Selecting class 3
# Calcualting mean for each class
Mn1 = np.mean(X[cl1Rows,], axis = 0)  
Mn2 = np.mean(X[cl2Rows,], axis = 0)
Mn3 = np.mean(X[cl3Rows,], axis = 0)
# Calculating std deviation for each class
Std1 = np.cov(X[cl1Rows,].T)
Std2 = np.cov(X[cl2Rows,].T)
Std3 = np.cov(X[cl3Rows,].T)
# Plotting gaussian normal distribution for each class using normald function
gaussians = np.array([normald(X,mu.reshape((-1,1)),sigma) for mu,sigma in [(Mn1,Std1), (Mn2,Std2), (Mn3,Std3)]])
gaussians = gaussians.squeeze()
gaussians.shape

plt.figure(figsize = (8,6))
plt.plot(gaussians.flat);
plt.xlabel('xs')
plt.ylabel('Probability distribution')
plt.legend(('Data',), loc = 'best')
plt.savefig('d52')

# Analysis using LDA
def discLDA(X, standardize, mu, Sigma1, prior):
    Xc = standardize(X)
    if Sigma1.size == 1:
        Sigma1 = np.asarray(Sigma1).reshape((1,1))
    det = np.linalg.det(Sigma1)        
    if det == 0:
        raise np.linalg.LinAlgError('discLDA(): Singular covariance matrix')
    Sigma = prior * Sigma1
    SigmaInv = np.linalg.inv(Sigma)
    return (mu * np.dot(Xc,SigmaInv)) - (0.5 * np.sum(np.dot(mu,SigmaInv) * mu)) + np.log(prior)

trainF = 0.8 # training fraction
# Selecting each of the classes with target values 0, 1 and 2
p1I,_ = np.where(T == 0)
p2I,_ = np.where(T == 1)
p3I,_ = np.where(T == 2)
# The target rows are shuffled
p1I = np.random.permutation(p1I)
p2I = np.random.permutation(p2I)
p3I = np.random.permutation(p3I)
# No. of indices for each of the classes
np1 = round(trainF * len(p1I))
np2 = round(trainF * len(p2I))
np3 = round(trainF * len(p3I))
# Selecting the training rows
rowsTrain = np.hstack((p1I[:np1], p2I[:np2], p3I[:np3]))
Xtrain = X[rowsTrain,:] # Defining Xtrain
Ttrain = T[rowsTrain,:] # Defining Ttrain
# Selecting the testrows
rowsTest = np.hstack((p1I[np1:], p2I[np2:], p3I[np3:]))
Xtest = X[rowsTest,:] # Defining Xtest
Ttest = T[rowsTest,:] # Defining Ttest

Xtrain.shape, Ttrain.shape, Xtest.shape, Ttest.shape

standardize,_ = makeStandardize(Xtrain)
Xtrains = standardize(Xtrain)
Xtests = standardize(Xtest)

Ttr = (Ttrain == 0).reshape((-1))
mu1 = np.mean(Xtrains[Ttr,:], axis = 0)
cov1 = np.cov(Xtrains[Ttr,:].T)
Ttr = (Ttrain.ravel() == 1).reshape((-1))
mu2 = np.mean(Xtrains[Ttr,:], axis = 0)
cov2 = np.cov(Xtrains[Ttr,:].T)
Ttr = (Ttrain.ravel() == 2).reshape((-1))
mu3 = np.mean(Xtrains[Ttr,:], axis = 0)
cov3 = np.cov(Xtrains[Ttr,:].T)

mu1, mu2, mu3, cov1, cov2, cov3,np1, np2, np3

d1 = discLDA(Xtrains, standardize, mu1, cov1, float(np1)/(np1 + np2 + np3))
d2 = discLDA(Xtrains, standardize, mu2, cov2, float(np2)/(np1 + np2 + np3))
d3 = discLDA(Xtrains, standardize, mu3, cov3, float(np3)/(np1 + np2 + np3))
predictedTrain = np.argmax(np.hstack((d1, d2, d3)).T, axis = 0)

d1.shape, d2.shape, d3.shape, predictedTrain.shape

d1t = discLDA(Xtests, standardize, mu1, cov1, float(np1)/(np1 + np2 + np3))
d2t = discLDA(Xtests, standardize, mu2, cov2, float(np2)/(np1 + np2 + np3))
d3t = discLDA(Xtests, standardize, mu3, cov3, float(np3)/(np1 + np2 + np3))
predictedTest = np.argmax(np.hstack((d1t, d2t, d3t)).T, axis = 0)

d1t.shape, d2t.shape, d3t.shape, predictedTest.shape

def percentCorrect(p, t):
    return np.sum(p.ravel()==t.ravel()) / float(len(t)) * 100

print ('Percent correct: Ttrain', percentCorrect(predictedTrain, Ttrain),'Ttest',percentCorrect(predictedTest, Ttest))

plt.figure(figsize = (8,6))
xs = Xtrain[:,0:1]
print (xs.shape)
order = np.argsort(xs[:,0])
y = np.hstack((d1, d2, d3))
print (y.shape)
plt.plot(xs[order], y[order]);
plt.ylabel("LDA Discriminant Functions")
plt.xlabel('xs')
plt.legend(('d1','d2','d3'), loc = 'best')
plt.savefig('d510')

a = np.log(np.array([[np1,np2,np3]]))
b = np.hstack((d1,d2,d3)).T
# Plot generative distributions  p(x | Class=k)  starting with discriminant functions
probs = np.exp( np.hstack((d1,d2,d3)) - 0.5*D*np.log(2*np.pi) - np.log(np.array([[np1,np2,np3]])))
probs.shape

probs = np.hstack((probs))
probs.shape

order = np.argsort(Xtrain[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtrain[order],probs[order],'-',alpha = 0.8);
plt.ylabel("LDA P(x|Class=k)\n from disc funcs", multialignment="center")
plt.xlabel('Xtest')
plt.legend(('$P(Class 1)$','$P(Class 2)$','$P(Class 3)$'))
plt.savefig('d511')

# Plot generative distributions  p(x | Class=k)  using normald
XS = standardize(Xtrain)
probs = np.hstack((normald(XS,mu1,cov1), normald(XS,mu2,cov2), normald(XS,mu3,cov3)))
order = np.argsort(Xtrain[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtrain[order],probs[order],'-',alpha = 0.8);
plt.ylabel("LDA P(x|Class=k)\n using normald", multialignment="center");
plt.xlabel('Xtest')
plt.legend(('$P(Class 1)$','$P(Class 2)$','$P(Class 3)$'))
plt.savefig('d512')

plt.figure(figsize = (8,6))
order = np.argsort(Xtrain[:,0])
plt.plot(Xtrain[order], predictedTrain[order], '-', alpha = 0.98)
plt.plot(Xtrain, Ttrain, 'o', alpha = 0.45)
plt.ylim(-0.2, 2.2)
plt.ylabel('Target Class')
plt.xlabel('X Values')
plt.legend(('Predicted Train', 'Actual Train'), loc = 'best')
plt.savefig('d513')

plt.figure(figsize = (8,6))
order = np.argsort(Xtest[:,0])
plt.plot(Xtest[order], predictedTest[order], '-', alpha = 0.98)
plt.plot(Xtest, Ttest, 'o', alpha = 0.45)
plt.ylim(-0.2, 2.2)
plt.ylabel('Target Class')
plt.xlabel('X Values')
plt.legend(('Predicted Test', 'Actual Test'), loc = 'best')
plt.savefig('d514')

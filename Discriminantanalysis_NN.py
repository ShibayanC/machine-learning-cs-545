import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
%matplotlib inline

!head iris.data

%precision 2
dataSet = pd.read_csv(open('iris.data'), header = None)
data = dataSet.values
print (data[0:150].shape)

def makeStandardize(X):
    means = X.mean(axis=0)
    stds = X.std(axis=0)
    def standardize(origX):
        return (origX - means) / stds
    def unStandardize(stdX):
        return stds * stdX + means
    return (standardize, unStandardize)

f = open("iris.data","r")
data = np.loadtxt(f ,delimiter=',', usecols=np.arange(4))
xs = data[:,2:3]
standardize,_ = makeStandardize(xs)

xs = standardize(xs)
xs.mean(0), xs.std(0)

t1 = [0]*50
t2 = [1]*50
t3 = [2]*50

ts = np.vstack((np.hstack((t1, t2, t3))))
xs.shape, ts.shape

Xdata = np.vstack((np.hstack((xs,ts))))
print ('Size of DataSet: ',Xdata.shape)
print (len(Xdata))
    
names = ['Petal-length','Type']
print (Xdata[0:5])

X = Xdata[:,0:1]
T = Xdata[:,1:2]
print ('X.shape & T.shape: ',X.shape, T.shape)

import neuralnetworks1 as nn
import mpl_toolkits.mplot3d as plt3
from matplotlib import cm

X.shape, T.shape

trainF = 0.8

p1I,_ = np.where(T == 0)
p2I,_ = np.where(T == 1)
p3I,_ = np.where(T == 2)

p1I = np.random.permutation(p1I)
p2I = np.random.permutation(p2I)
p3I = np.random.permutation(p3I)

np1 = round(trainF * len(p1I))
np2 = round(trainF * len(p2I))
np3 = round(trainF * len(p3I))

rowsTrain = np.hstack((p1I[:np1], p2I[:np2], p3I[:np3]))
Xtrain = X[rowsTrain,:]
Ttrain = T[rowsTrain,:]
rowsTest = np.hstack((p1I[np1:], p2I[np2:], p3I[np3:]))
Xtest = X[rowsTest,:]
Ttest = T[rowsTest,:]

Xtrain.shape, Ttrain.shape, Xtest.shape, Ttest.shape, X.shape, T.shape

nHidden = 5
nIterations = 0
nnet = nn.NeuralNetworkClassifier(1,nHidden,3)
nnet.train(Xtrain, Ttrain, nIterations = 1000)

xs = np.linspace(0,30,40)
Ytest = nnet.use(Xtest)
predTest,probs,_ = nnet.use(Xtest, allOutputs=True)
predTest.shape, probs.shape, Xtest.shape

order = np.argsort(Xtest[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtest[order],predTest[order],'-', alpha = 0.85)
plt.plot(Xtest[order],Ttest[order],'o', alpha = 0.45)
plt.ylim(-0.2, 2.2)
plt.xlabel('Xtest')
plt.ylabel('Predicted Test')
plt.legend(('Predicted Test','Actual Test'), loc = 'best')
plt.savefig('d524')

#probs = np.hstack((probs)).T
probs.T.shape

order = np.argsort(Xtest[:,0])
plt.figure(figsize = (8,6))
plt.plot(Xtest[order], probs[order])
plt.ylim(-0.1, 1.1)
plt.xlabel('Xtest')
plt.ylabel('Probability Distribution')
plt.legend(('$P(C=1)$','$P(C=2)$','$P(C=3)$'),loc='best')
plt.savefig('d525')

def percentCorrect(p,t):
    return np.sum(p.ravel()==t.ravel()) / float(len(t)) * 100

print("LogReg: Percent correct: Test {:.3g}".format(percentCorrect(predTest,Ttest)))

plt.figure(figsize=(8,6))
plt.plot(np.exp(-nnet.getErrorTrace()))
plt.xlabel("Epochs")
plt.ylabel("Likelihood")
plt.xlim(-1,100)
plt.savefig('d526')

plt.figure(figsize=(8,6))
nnet.draw()
plt.savefig('d527')


colors = ['red','green','blue']
plt.figure(figsize=(8,6))
c = 0
plt.figure(figsize=(8,6))
for c in range(0,3):
    mask = (T == c).flatten()
    plt.plot(Xdata[mask,0],Xdata[mask,1],'o',markersize=6, alpha=0.5, color=colors[c-1])
plt.ylim(-0.2, 2.2)
plt.savefig('d528')


predTest = predTest.squeeze()
plt.figure(figsize=(8,6))
plt.plot(Xtest[:,0],predTest,'o')
plt.ylim(-0.2, 2.2)
plt.xlabel('Xtest')
plt.ylabel('predictedTest')
plt.legend(('Data',),loc = 'best')
plt.savefig('d529')

fig = plt.figure(figsize=(10,10))
c = 0
for c in range(0,3):
    ax = fig.add_subplot(3,1,c+1)
    ax.plot(probs[:,c])
    ax.xlabel('Xtest')
    ax.ylabel('Probability Distribution')
plt.savefig('d530')

plt.rcParams['figure.figsize'] = (10, 10)

run neuralnetworks1

plt.savefig('d527')

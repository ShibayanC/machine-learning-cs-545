import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
%matplotlib inline

!wget https://archive.ics.uci.edu/ml/machine-learning-databases/abalone/abalone.data
! head abalone.data

dataSet = pd.read_csv(open('abalone.data'))
dataSet = pd.read_csv('/home/shibayan/anaconda3/notebooks/abalone.data', header=None)
dataSet = pd.read_csv('/home/shibayan/anaconda3/notebooks/abalone.data', names = ['Sex','Len','Dia','Height',
                            'WW(gms)','SW(gms)', 'VW(gms)','ShW(gms)','Rings'])
print (dataSet[:5])

# Reference:
# Sex: defines the sex of the abalones
# Len: Length of the abalones (mm)
# Dia: Diameter of the abalone (mm)
# WW: Whole Weight of the abalone (gms)
# SW: Shucked Weight of the abalone (gms)
# VW: Viscera weight of the abalone (gms)
# ShW: Shell Weight of the abalone (gms)
# Rings: Nos of rings on the abalone

# Data used for the analysis: Len, Dia, Height
data = dataSet[['Len','Dia','Height']].values
data.shape

xs = data[0:,0:1]
ys = data[0:,1:2]
zs = data[0:,2:3]
print ('Length of data set: ', len(xs))
print ('Size of data set: 4177 X 3')
# Plotting the 3D data set
# Plotting the abalone data in terms of Length, Diameter, Height
fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(xs, ys, zs, c = 'b', marker = 'o')
ax.set_xlabel('Length (mm)')
ax.set_ylabel('Diameter (mm)')
ax.set_zlabel('Height (mm)')
fig.set_size_inches(10, 10)
fig.savefig('3D_plot2.2_.png',dpi=100)
ax.set_title('Abalone data-set (len, dia, ht)')
#ax.set_title('Size of data set: 4177 X 3')
plt.show()

# Code for printing the normal distribution
def normald(dataSample, Mu, Sigma):
    # dataSample contains the working data of dimension N x D (N is the nos of data sets, D is the nos of attributes)
    # Mu is mean vector, of dimension k x D (k is the nos of mean, D is the nos of mean of each mean)
    # Sigma is covariance matrix, of dimension D x D (D is the nos of attributes, square matrix)
    D = dataSample.shape[1]
    detmSigma = Sigma if D == 1 else np.linalg.det(Sigma) # Calculating determinant of 'Sigma'
    if detmSigma == 0:
        print("\n Mu value: ",Mu)
        print("\n Sigma value: ",Sigma)
        raise np.linalg.LinAlgError('normald(): Singular covariance matrix') # Error if the determinant of Sigma is zero
    sigmaInverse = 1.0/Sigma if D == 1 else np.linalg.inv(Sigma)
    normConstant = 1.0 / np.sqrt((2*np.pi)**D * detmSigma) # Calculating the normalization constant
    diffVector = dataSample - Mu.T # Change column vector Mu to be row vector, calculating vector difference
    return normConstant * np.exp(-0.5 * np.sum(np.dot(diffVector, sigmaInverse) * diffVector, axis=1))[:,np.newaxis]

# Code for having the mixture of Gaussian
def mixOfGauss(dataSample, k, nIter = 10, verbose = True):
    # dataSample is N x D matrix of data samples, having N datasets each with D attributes
    # k is nos of means to be considered
    # nIter is the nos of iterations for the EM step
    # Mu is mean vector, of dimension k x D (k is the nos of mean, D is the nos of mean of each mean)
    # Sigma is covariance matrix, of dimension D x D (D is the nos of attributes, square matrix)
    # Pi is k x 1 vector of weightings for each Gaussian ("k depends upon the number of attributes of the dataset")
    # Gamma is k x N array of responsibilities of Gaussian k for sample n
    N = 0
    D = 0
    N,D = dataSample.shape # N: nos of attributes; D: nos of attributes of each sample
    randomSample = np.random.choice(range(N),k,replace=False)
    Mu = dataSample[randomSample,:] # Selecting any random sample of the dataset to be considered as mean
    print("\n Random Sample: ", randomSample) # row Indices, considered as random sample  
    print("\n Initial Mu: ", Mu) # Initial Mu
    Sigma = np.tile(np.eye(D),k).T.reshape((k,D,D))
    Pi = np.tile(1/k, k).reshape((-1,1))
    
    for i in range(nIter):
        # Expectation Step
        Gauss = np.array([normald(dataSample,mu.reshape((-1,1)),sigma) for mu,sigma in zip(Mu,Sigma)])
        Gauss = Gauss.squeeze()
        Gamma = ((Pi *Gauss) / np.sum(Pi * Gauss, axis=0))
        Gamma[np.isnan(Gamma)] = 0 # to take care of entries where above denominator is zero
        
        # Maximization Step, 
        Nk = np.sum(Gamma,axis=1).reshape((-1,1))
        Mu = np.dot(Gamma,dataSample) / Nk
        diffsk = dataSample[:,np.newaxis,:] - Mu
        for j in range(k):
            Sigma[j,:,:] = np.dot(Gamma[j,:] * diffsk[:,j,:].T, diffsk[:,j,:]) / Nk[j]
        Pi = Nk / np.sum(Nk)
        Pi = Pi.reshape((-1,1))
        if verbose:
            for j in range(k):
                print("\n Gaussian: ",j)
                print("\n Mu: ",Mu[j,:])
                print("\n Sigma: ",Sigma[j,:,:])
                print("\n Pi: ",Pi[j])
                #print("\n Gamma: ", Gamma)
            
    return Mu, Sigma, Pi, Gamma

m,sig,p,g = mixOfGauss(data,2,nIter = 3)

nos = int(input("\n Enter the nos of initial indices: ")) # User input for the nos of initial indices
print ("\n Nos of clusters to be formed: ",nos) # Nos of initial indices will be the same as the nos of clusters
nIter = int(input("\n Enter the nos iterations: ")) # User input for the nos of iterations
m,sig,p,g = mixOfGauss(data, nos, nIter, verbose = False)

# Plotting of 3-D plot of the clusters
fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(xs, ys, zs, alpha = 0.5, s = 50, c = np.argmax(g,axis=0))
#data = dataset[['Time', 'Duration', 'Height']].values
ax.set_xlabel('Length')
ax.set_ylabel('Diameter')
ax.set_zlabel('Height')
fig.set_size_inches(10, 10)
fig.savefig('Cluster_plot_demo2.2.png',dpi=100)
plt.show()

#----------------------------------LOG LIKLIHOOD------------------------------#
# Mixture of Gaussians for implementation of "log-likelihood"
# The log function is used such that the evaluation of maximization becomes simple 
def mixOfGauss(dataSample, k, nIter = 10, verbose = True):
    # dataSample is N x D matrix of data samples, having N datasets each with D attributes
    # k is nos of means to be considered
    # nIter is the nos of iterations for the EM step
    # Mu is mean vector, of dimension k x D (k is the nos of mean, D is the nos of mean of each mean)
    # Sigma is covariance matrix, of dimension D x D (D is the nos of attributes, square matrix)
    # Pi is k x 1 vector of weightings for each Gaussian ("k depends upon the number of attributes of the dataset")
    # Gamma is k x N array of responsibilities of Gaussian k for sample n
    N = 0
    D = 0
    lli = 0
    logLH = []
    N,D = dataSample.shape # N: nos of attributes; D: nos of attributes of each sample
    randomSample = np.random.choice(range(N),k,replace=False)
    Mu = dataSample[randomSample,:] # Selecting any random sample of the dataset to be considered as mean
    print("\n Random Sample: ", randomSample) # row Indices, considered as random sample  
    print("\n Initial Mu: ", Mu) # Initial Mu
    Sigma = np.tile(np.eye(D),k).T.reshape((k,D,D))
    Pi = np.tile(1/k, k).reshape((-1,1))
    
    for i in range(nIter):
        # Expectation Step
        Gauss = np.array([normald(dataSample,mu.reshape((-1,1)),sigma) for mu,sigma in zip(Mu,Sigma)])
        Gauss = Gauss.squeeze()
        Gamma = ((Pi *Gauss) / np.sum(Pi * Gauss, axis=0))
        Gamma[np.isnan(Gamma)] = 0 # to take care of entries where above denominator is zero
        
        # Maximization Step, 
        Nk = np.sum(Gamma,axis=1).reshape((-1,1))
        Mu = np.dot(Gamma,dataSample) / Nk
        diffsk = dataSample[:,np.newaxis,:] - Mu
        for j in range(k):
            Sigma[j,:,:] = np.dot(Gamma[j,:] * diffsk[:,j,:].T, diffsk[:,j,:]) / Nk[j]
        Pi = Nk / np.sum(Nk)
        Pi = Pi.reshape((-1,1))
        lli = np.sum(np.log(np.sum(Pi * Gauss, axis=0))) # log likelihood for the gaussian function
        logLH.append(lli)
        if verbose:
            for j in range(k):
                print("\n Gaussian: ",j)
                print("\n Mu: ",Mu[j,:])
                print("\n Sigma: ",Sigma[j,:,:])
                print("\n Pi: ",Pi[j])
    return Mu, Sigma, Pi, Gamma, np.array(logLH)

nos = int(input("\n Enter the nos of initial indices: ")) # User input for the nos of initial indices
print ("\n Nos of clusters to be formed: ",nos) # Nos of initial indices will be the same as the nos of clusters
nIter = int(input("\n Enter the nos iterations: ")) # User input for the nos of iterations
m,sig,p,g,ll = mixOfGauss(data, nos, nIter, verbose = False)
print ("\n Nos of indices of Log-Liklihood: ", ll.shape)
# Plotting of 3-D plot of the clusters
fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(xs, ys, zs, alpha = 0.5, s = 50, c = np.argmax(g,axis=0))
ax.set_xlabel('Length')
ax.set_ylabel('Diameter')
ax.set_zlabel('Height')
fig.set_size_inches(8, 8)
fig.savefig('Cluster_plot2.2.png',dpi=100)
plt.show()

nos = int(input("\n Enter the nos of initial indices: ")) # User input for the nos of initial indices
print ("\n Nos of clusters to be formed: ",nos) # Nos of initial indices will be the same as the nos of clusters
nIter = int(input("\n Enter the nos iterations: ")) # User input for the nos of iterations
m,sig,p,g,ll = mixOfGauss(data, nos, nIter, verbose = False)

fig = plt.figure()
ax = fig.add_subplot(111, projection = '3d')
ax.scatter(xs, ys, zs, alpha = 0.5, s = 50, c = np.argmax(g,axis=0))
ax.set_xlabel('Length')
ax.set_ylabel('Diameter')
ax.set_zlabel('Height')
fig.set_size_inches(8, 8)
fig.savefig('demo71.png',dpi=100)
plt.show()

fig = plt.figure()
plt.plot(np.exp(ll/data.shape[0]),'-o');
plt.xlabel('Iterations');
plt.ylabel('Likelihood');
fig.set_size_inches(8,8)
fig.savefig('demo72.png',dpi=100)

fig = plt.figure()
plt.hist(g.T,color=('blue','red','green','black'))
plt.ylabel('$\gamma$');
plt.legend(('First Gaussian $\gamma$','Second Gaussian $\gamma$', 'Third Gaussian $\gamma$', 'Fourth Gaussian $\gamma$'));
fig.savefig('a10.png',dpi=100)

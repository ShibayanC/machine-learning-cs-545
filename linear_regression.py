import numpy as np
import pandas as pd
import random
import IPython.display as ipd  # for display and clear_output
import time  # for sleep
import matplotlib.pyplot as plt
%matplotlib inline

! wget http://archive.ics.uci.edu/ml/machine-learning-databases/concrete/slump/slump_test.data
! head slump_test.data

%precision 2
dataSet = pd.read_csv(open('slump_test.data'))
dataSet = dataSet[['Cement','Slag','Fly ash','Water','SP','Coarse Aggr.','Fine Aggr.',
                   'SLUMP(cm)','FLOW(cm)','Compressive Strength (28-day)(Mpa)']]
#print (dataSet)
data = dataSet.values
print (data[0:5,])
print ("Size of Dataset: ",data.shape)

T = data[:, 7:10]
print ("Test set dimension: ",T.shape)
X = data[:, 0:7]
print ("Input set dimension: ",X.shape)

parn = random.randrange(1, 100) 
partn = (parn/100)                  # Converting partition percentage
print ("Partition percentage: ",partn)                       # Partition value

nrows = X.shape[0]
nTrain = int(round(nrows*partn))
nTest = nrows - nTrain
print ("Dimensions based on partition 'partn': ", partn,nTrain,nTest,nTrain+nTest)

rows = np.arange(nrows)
np.random.shuffle(rows)

trainIndices = rows[:nTrain]
testIndices = rows[nTrain:]

Xtrain = X[trainIndices,:]
Ttrain = T[trainIndices,:]
Xtest = X[testIndices,:]
Ttest = T[testIndices,:]


print ("Dimension of the training set (input & output): ", Xtrain.shape,Xtest.shape)
print ("Dimension of the test set (input & output)", Ttrain.shape,Ttest.shape)
print (Xtrain[0:5,])
print (Xtest[0:5,])

X1train = np.hstack(( np.ones((nTrain,1)), Xtrain))
X1test = np.hstack(( np.ones((nTest,1)), Xtest))
print (X1train.shape, X1test.shape, Ttrain.shape, Ttest.shape)

ntrSamples = X1train.shape[1]
ncols = X1train.shape[1]
result = []
lmdas = np.linspace(0,1000,50)*ntrSamples
for lmda in lmdas:
    lmbdaI = lmda * np.eye(ncols)
    lmbdaI[0,0] = 0
    wt = np.linalg.lstsq(np.dot(X1train.T,X1train) + lmbdaI, np.dot(X1train.T, Ttrain))[0]
    predTrain = np.dot(X1train,wt)
    predTest = np.dot(X1test,wt)
    result.append([lmda, np.sqrt(np.mean((predTrain-Ttrain)**2)),
                   np.sqrt(np.mean((predTest-Ttest)**2)),
                   list(wt.flatten())])
    if lmda == 0:
        fitTrain = np.hstack((Ttrain,predTrain))
        fitTest = np.hstack((Ttest,predTest))
    if lmda == lmdas[-1]:
        fitTrainLast = np.hstack((Ttrain,predTrain))
        fitTestLast = np.hstack((Ttest,predTest))

lmdas = [res[0] for res in result]
rmses = np.array([res[1:3] for res in result]) 
wt = np.array( [res[3] for res in result] )

print (len(lmdas))
print (len(rmses))
print (len(wt))

plt.figure(figsize=(20,20))
plt.subplot(4,2,1)
plt.plot(fitTrain[:,0],fitTrain[:,1],'o')
a,b = max(np.min(fitTrain,axis=0)), min(np.max(fitTrain,axis=0)) 
plt.plot([a,b],[a,b],'r',linewidth=3)
plt.title('Training result, with lambda = 0')
plt.xlabel('Actual')
plt.ylabel('Predicted')

plt.subplot(4,2,3)
plt.plot(fitTest[:,0],fitTest[:,1],'o')
a,b = max(np.min(fitTest,axis=0)), min(np.max(fitTest,axis=0)) 
plt.plot([a,b],[a,b],'r',linewidth=3)
plt.title('Testing result, with lambda = 0')
plt.xlabel('Actual')
plt.ylabel('Predicted')


plt.subplot(4,2,2)
plt.plot(fitTrainLast[:,0],fitTrainLast[:,1],'o')
a,b = max(np.min(fitTrainLast,axis=0)), min(np.max(fitTrainLast,axis=0)) 
plt.plot([a,b],[a,b],'r',linewidth=3)
plt.title('Training result, with lambda = ' + str(lmdas[-1]))
plt.xlabel('Actual')
plt.ylabel('Predicted')

plt.subplot(4,2,4)
plt.plot(fitTestLast[:,0],fitTestLast[:,1],'o')
a,b = max(np.min(fitTestLast,axis=0)), min(np.max(fitTestLast,axis=0)) 
plt.plot([a,b],[a,b],'r',linewidth=3)
plt.title('Testing result, with lambda = ' + str(lmdas[-1]))
plt.xlabel('Actual')
plt.ylabel('Predicted')

plt.subplot(4,2,(5,6))
plt.plot(lmdas,rmses,'o-')
plt.legend(('train','test'))
plt.ylabel('RMSE')
plt.xlabel('$\lambda$')
plt.subplot(4,2,(7,8))
plt.plot(lmdas,wt,'o-')
plt.plot([0,max(lmdas)], [0,0], 'k--')
plt.legend(('$w_0$','$w_1$','$w_2$','$w_3$','$w_4$','$w_5$','$w_6$','$w_7$'))
plt.ylabel('weights')
plt.xlabel('$\lambda$')
plt.savefig('d4')
print(len(wt))

meanLamda = []
meanRMSEStrain = []
meanRMSEStest = []
npart = int(input("Enter the nos of partitions: "))
count = 1
for count in range(1, npart+1):
    parn = random.randrange(1, 100) 
    partn = (parn/100)                  # Converting partition percentage
    
    nrows = X.shape[0]
    nTrain = int(round(nrows*partn))
    nTest = nrows - nTrain
    #print ("Dimensions based on partition 'partn': ", partn,nTrain,nTest,nTrain+nTest)
    
    rows = np.arange(nrows)
    np.random.shuffle(rows)

    trainIndices = rows[:nTrain]
    testIndices = rows[nTrain:]

    Xtrain = X[trainIndices,:]
    Ttrain = T[trainIndices,:]
    Xtest = X[testIndices,:]
    Ttest = T[testIndices,:]
    #print (Xtrain.shape, Xtest.shape, Ttrain.shape, Ttest.shape)
    
    X1train = np.hstack(( np.ones((nTrain,1)), Xtrain))
    X1test = np.hstack(( np.ones((nTest,1)), Xtest))
    
    result = []
    #print (X1train.shape, X1test.shape, Ttrain.shape, Ttest.shape)
    ntrSamples = X1train.shape[0]
    nteSamples = X1test.shape[0]
        
    lmdas = np.linspace(0,100,100)*ntrSamples
    for lmda in lmdas:
        lmbdaI = lmda * np.eye(ncols)
        lmbdaI[0,0] = 0
        wt = np.linalg.lstsq(np.dot(X1train.T,X1train) + lmbdaI, np.dot(X1train.T, Ttrain))[0]
        predTrain = np.dot(X1train,wt)
        predTest = np.dot(X1test,wt)
        result.append([lmda, np.sqrt(np.mean((predTrain-Ttrain)**2)),
                   np.sqrt(np.mean((predTest-Ttest)**2)),
                   list(wt.flatten())])
        if lmda == 0:
            fitTrain = np.hstack((Ttrain,predTrain))
            fitTest = np.hstack((Ttest,predTest))
        if lmda == lmdas[-1]:
            fitTrainLast = np.hstack((Ttrain,predTrain))
            fitTestLast = np.hstack((Ttest,predTest))

    lmdas = [res[0] for res in result]
    rmses = np.array([res[1:3] for res in result]) 
    wt = np.array( [res[3] for res in result] )
    mnLamda = np.mean(lmdas)
    mnrmsestrain = np.mean(rmses)
    mnrmsestest = np.mean(wt)
    #print ("Mean of 'Lambdas' for evaluation nos: " + str(count) + " is: " + str(mnLamda))
    meanLamda.append(mnLamda)
    #print ("Mean of 'RMSES (train)' for evaluation nos: " + str(count) + " is: " + str(mnrmsestrain))
    meanRMSEStrain.append(mnrmsestrain)
    #print ("Mean of 'RMSES (test)' for evaluation nos: " + str(count) + " is: " + str(mnrmsestest))
    meanRMSEStest.append(mnrmsestest)
   
#print (len(lmdas))
#print (len(rmses))
#print (len(wt))
#print (len(meanLamda))
#print ("Mean of all 'lamdas': ",meanLamda)
#print ("Mean of all 'rmses (train)': ",meanRMSEStrain)
#print ("Mean of all 'rmses (test)': ",meanRMSEStest)

minmeanRMSEStest = np.min(meanRMSEStest)
print (minmeanRMSEStest)
minIndex = [i for i,x in enumerate(meanRMSEStest) if x == minmeanRMSEStest]
print (minIndex)
newLamda = meanLamda[minIndex[0]]  # Lamda value for which the difference between the RMSES train value & test vakue is min 
print ("The lamda taken for consideration: ", newLamda)

meanLamdas = np.sort(meanLamda) # For printing in a sequential order

plt.figure(figsize=(10,6))
plt.plot(meanLamdas, meanRMSEStrain, label="meanRMSES (train)", lw=0.5, marker='+')
plt.plot(meanLamdas, meanRMSEStest, label="meanRMSES (test)", lw=0.5, marker='*')
plt.legend(loc=1) # upper left corner
plt.xlabel('$\lambda$', fontsize=18)
plt.ylabel('meanRMSES', fontsize=18)
plt.title('RMSES (training set, testing set)    VS        $\lambda$');
plt.savefig('d5')
plt.show();

parn = random.randrange(1, 100) 
partn = (parn/100)

nrows = X.shape[0]
nTrain = int(round(nrows*partn))
nTest = nrows - nTrain

print ("Testing the outputs for a 'lamba' value: " + str(newLamda) + " partiton value: " + str(partn))

rows = np.arange(nrows)
np.random.shuffle(rows)

trainIndices = rows[:nTrain]
testIndices = rows[nTrain:]

Xtrain = X[trainIndices,:]
Ttrain = T[trainIndices,:]
Xtest = X[testIndices,:]
Ttest = T[testIndices,:]
    
X1train = np.hstack(( np.ones((nTrain,1)), Xtrain))
X1test = np.hstack(( np.ones((nTest,1)), Xtest))

print ("Dimension of the training set (input & output): ", X1train.shape,Ttrain.shape)
print ("Dimension of the test set (input & output)", X1test.shape,Ttest.shape)

nCTrain = Ttrain.shape[1]         # No. of columns of training dataset
nCTest = Ttest.shape[1]           # No. of columns of testing dataset

ntrSamples = Xtrain.shape[0]
nteSamples = Xtest.shape[0]

#print (X1train[0:5,])
#print (X1test[0:5,])

lmbdaI = newLamda * np.eye(1)
lmbdaI[0,0] = 0
wt = np.linalg.lstsq(np.dot(X1train.T,X1train) + lmbdaI, np.dot(X1train.T, Ttrain))[0]
predTrain = np.dot(X1train,wt)
predTest = np.dot(X1test,wt)

fitTrain = np.hstack((Ttrain,predTrain))
fitTest = np.hstack((Ttest,predTest))

fitTrainLast = np.hstack((Ttrain,predTrain))
fitTestLast = np.hstack((Ttest,predTest))

print (predTrain.shape)
print (predTest.shape)

print (Ttrain.shape)
print (Ttest.shape)
#print (predTrain[0:5,])
#print (predTest[0:5,])

fig = plt.figure()
B1 = predTrain[:,0:1]
B2 = predTest[:,0:1]
A1 = Ttrain[:,0:1]
A2 = Ttest[:,0:1]
a = max(min(B1),min(A1))
b = min(max(B1),max(A1))
plt.scatter(A1, B1, c='blue', marker = 'o',alpha = 0.5, s = 50)
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Train')
plt.ylabel('Prediction Train')
plt.legend(('model', 'data'))
plt.title('SLUMP dataset')
fig.savefig('d6')
plt.show()

fig = plt.figure()
a = max(min(B2),min(A2))
b = min(max(B2),max(A2))
plt.scatter(A2, B2, c='blue', marker = 'o',alpha = 0.5, s = 50)
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Test')
plt.ylabel('Prediction Test')
plt.legend(('model', 'data'))
plt.title('SLUMP dataset')
fig.savefig('d7')
plt.show()

print (np.mean(A2.T))
print (np.mean(B2.T))
#print (A2.T, B2.T)
d = (np.mean(A2.T) - np.mean(B2.T))
diff = np.abs(d)
print (diff)
x = (diff/np.mean(A2.T))*100
print (x)

fig = plt.figure()
B1 = predTrain[:,1:2]
B2 = predTest[:,1:2]
A1 = Ttrain[:,1:2]
A2 = Ttest[:,1:2]
a = max(min(B1),min(A1))
b = min(max(B1),max(A1))
plt.scatter(A1, B1, c='blue', marker = 'o',alpha = 0.5, s = 50) 
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Train')
plt.ylabel('Prediction Train')
plt.legend(('model', 'data'))
plt.title('FLOW dataset')
fig.savefig('d8')
plt.show()

fig = plt.figure()
a = max(min(B2),min(A2))
b = min(max(B2),max(A2))
plt.scatter(A2, B2, c='blue', marker = 'o',alpha = 0.5, s = 50) 
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Test')
plt.ylabel('Prediction Test')
plt.legend(('model', 'data'))
plt.title('FLOW dataset')
fig.savefig('d9')
plt.show()

print (np.mean(A2.T))
print (np.mean(B2.T))
#print (A2.T, B2.T)
d = (np.mean(A2.T) - np.mean(B2.T))
diff = np.abs(d)
print (diff)
x = (diff/np.mean(A2.T))*100
print (x)

fig = plt.figure()
B1 = predTrain[:,2:3]
B2 = predTest[:,2:3]
A1 = Ttrain[:,2:3]
A2 = Ttest[:,2:3]
a = max(min(B1),min(A1))
b = min(max(B1),max(A1))
plt.scatter(A1, B1, c='blue', marker = 'o',alpha = 0.5, s = 50) 
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Train')
plt.ylabel('Prediction Train')
plt.legend(('model', 'data'))
plt.title('Compressive Strn dataset')
fig.savefig('d10')
plt.show()

fig = plt.figure()
a = max(min(B2),min(A2))
b = min(max(B2),max(A2))
plt.scatter(A2, B2, c='blue', marker = 'o',alpha = 0.5, s = 50) 
plt.plot([a,b],[a,b], 'r', linewidth=3,alpha=0.7);
plt.xlabel('Actual Test')
plt.ylabel('Prediction Test')
plt.legend(('model', 'data'))
plt.title('Compressive Strn dataset')
fig.savefig('d11')
plt.show()

print (np.mean(A2.T))
print (np.mean(B2.T))
#print (A2.T, B2.T)
d = (np.mean(A2.T) - np.mean(B2.T))
diff = np.abs(d)
print (diff)
x = (diff/np.mean(A2.T))*100
print (x)

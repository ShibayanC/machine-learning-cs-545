#------------------------Sammon Principle-----------------------#

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
% matplotlib inline

def diffTODist(dX):
    return np.sqrt(np.sum(dX*dX, axis = 1))

def proj(X, theta):
    return np.dot(X, theta)

def objective(X, proj, theta, s):
    N = X.shape[0]
    P = proj(X, theta)
    dX = np.array([X[i,:] - X[j,:] for i in range(N-1) for j in range(i+1, N)])
    dP = np.array([P[i,:] - P[j,:] for i in range(N-1) for j in range(i+1, N)])
    return 1/N * np.sum(((diffTODist(dX)/s) - diffTODist(dP))**2)

def gradient(X, proj, theta, s):
    N = X.shape[0]
    P = proj(X, theta)
    dX = np.array([X[i,:] - X[j,:] for i in range(N-1) for j in range(i+1, N)])
    dP = np.array([P[i,:] - P[j,:] for i in range(N-1) for j in range(i+1, N)])
    distX = diffTODist(dX)
    distP = diffTODist(dP)
    return -1/N * np.dot((((distX/s - distP) / distP).reshape((-1,1)) * dX).T, dP)

N, X = 79, 3 # Length of data is 79 X 3
data = np.random.random((N,X))
mean = np.mean(data, axis = 0)
print (mean)
print (np.cov(data.T).shape)
cov = np.cov(data.T)
Y = np.random.multivariate_normal(mean, cov)
print (Y)

n = 8
Y = np.vstack((Y,np.random.multivariate_normal([1,-1,1], 0.2*np.eye(3), n)))
Y = Y - np.mean(Y,axis=0)
s = 0.5 * np.sqrt(np.max(np.var(Y,axis=0)))
print('s',s)

u,svalues,v = np.linalg.svd(Y)
v = v.T
theta = v[:,:2]

nIterations = 10
vals = []
for i in range(nIterations):
    theta -= 0.001 * gradient(Y,proj,theta,s)
    v = objective(Y,proj,theta,s)
    vals.append(v)

print('theta\n',theta)
plt.figure(figsize=(10,15))
plt.subplot(3,1,(1,2))
P = proj(Y,theta)
mn = 1.1*np.min(Y)
mx = 1.1*np.max(Y)
plt.axis([mn,mx,mn,mx])
#strings = [chr(ord('a')+i) for i in range(X.shape[0])]
strings = [i for i in range(Y.shape[0])]
for i in range(Y.shape[0]):
    plt.text(Y[i,0],Y[i,1],strings[i],color='black',size=15)
    for i in range(P.shape[0]):
        plt.text(P[i,0],P[i,1],strings[i],color='green',size=15)
plt.title('2D data, Originals in black')

plt.subplot(3,1,3)
plt.plot(vals)
plt.ylabel('Objective Function');
plt.savefig('sammon_demo2.1.1.png',dpi=100)

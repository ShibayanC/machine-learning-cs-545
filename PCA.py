#---------------------Dimesionality Reduction (PCA)------------------------#

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline

def normalRand(n, mean, sigma):
    mean = np.array(mean)
    sigma = np.array(sigma)
    X = np.random.normal(0,1,n*len(mean)).reshape((n, len(mean)))
    return np.dot(X, np.linalg.cholesky(sigma)) + mean

def drawVector(Data,means,l,color,label): # Function to draw vector along the direction of propagation, vector from origin 
    l1 = means - Data*l/2 # Retrieving data of negative half part
    l2 = means + Data*l/2 # Retrieving data of positive half part
    Z = l1 + l2
    print ("\n Z.shape: ", Z.shape)
    plt.plot([l1[0],l2[0]],[l1[1],l2[1]],label=label,color=color,linewidth=2)
    
def plotPCA(data,V):
    plt.figure(figsize=(10,5))
    plt.subplot(1,2,1)
    plt.plot(data[:,0],data[:,1],'.')
    means = np.mean(data,axis=0)
    drawVector(V[:,1],means,8,"red","First")
    drawVector(V[:,2],means,8,"green","Second")
    leg = plt.legend()
    plt.axis('equal')
    plt.gca().set_aspect('equal')
    plt.subplot(1,2,2)    
    proj = np.dot(data - means, V)
    plt.plot(proj[:,0],proj[:,1],'.')
    plt.axis('equal')
    plt.gca().set_aspect('equal')
    plt.xlabel("First")
    plt.ylabel("Second")
    plt.title("Projected to First and Second Singular Vectors");
    plt.savefig('PCA_demo2.1.1.png',dpi=100)

N = len(data)
meanOfData = np.mean(data, axis = 0)
dataN = data - meanOfData
print (dataN.shape)
U, S, V = np.linalg.svd(dataN)
vMod = V.T
print (vMod)
print (vMod.shape)
plotPCA(data, vMod)

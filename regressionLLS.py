import numpy as np
import pandas as pd
import random
import math
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
%matplotlib inline

!head iris.data

%precision 2
dataSet = pd.read_csv(open('iris.data'), header = None)
data = dataSet.values
print (data[0:150].shape)

def makeStandardize(X):
    means = X.mean(axis=0)
    stds = X.std(axis=0)
    def standardize(origX):
        return (origX - means) / stds
    def unStandardize(stdX):
        return stds * stdX + means
    return (standardize, unStandardize)

f = open("iris.data","r")
data = np.loadtxt(f ,delimiter=',', usecols=np.arange(4))
xs = data[:,0:4]
standardize,_ = makeStandardize(xs)

xs = standardize(xs)
xs.mean(0), xs.std(0)

t1 = [0]*50
t2 = [1]*50
t3 = [2]*50

ts = np.vstack((np.hstack((t1, t2, t3))))
xs.shape, ts.shape

plt.figure(figsize = (8,6))
plt.plot(xs, ts, 'o')
plt.ylim(-0.3, 2.3)
plt.xlabel('Petal-Length')
plt.ylabel('Class-Type')
plt.legend(('Data',), loc = 'best')
plt.savefig('d501')

Xdata = np.vstack((np.hstack((xs,ts))))
print ('Size of DataSet: ',Xdata.shape)
print (len(Xdata))
    
names = ['Sepal-length','Sepal-width','Petal-length','Petal-Width','Type']
print (Xdata[0:5])

def normald(X, mu=None, sigma=None):
    d = X.shape[1]
    if np.any(mu == None):
        mu = np.zeros((d,1))
    if np.any(sigma == None):
        sigma = np.eye(d)
    detSigma = sigma if d == 1 else np.linalg.det(sigma)
    if detSigma == 0:
        raise np.linalg.LinAlgError('normald(): Singular covariance matrix')
    sigmaI = 1.0/sigma if d == 1 else np.linalg.inv(sigma)
    normConstant = 1.0 / np.sqrt((2*np.pi)**d * detSigma)
    diffv = X - mu.T # change column vector mu to be row vector
    return normConstant * np.exp(-0.5 * np.sum(np.dot(diffv, sigmaI) * diffv, axis=1))[:,np.newaxis]

def addOnes(A):
    return np.hstack((np.ones((A.shape[0],1)), A))

X = Xdata[:,0:4]
T = Xdata[:,4:5]
print ('X.shape & T.shape: ',X.shape, T.shape)

X.dtype

print('{:20s} {:^9s} {:^9s}'.format(' ','mean','stdev'))
for i in range(X.shape[1]):
    print('{:20s} {:^9.3g} {:^9.3g}'.format(names[i],np.mean(X[:,i]),np.std(X[:,i])))

uniq = np.unique(T)
print('   Value  Occurrences')
for i in uniq:
    print('{:7.1g} {:10d}'.format(i, np.sum(T==i)))

trF = 0.8    # Training fraction
p1I,_ = np.where(T == 0)   
p2I,_ = np.where(T == 1)   
p3I,_ = np.where(T == 2)
p1I = np.random.permutation(p1I)
p2I = np.random.permutation(p2I)
p3I = np.random.permutation(p3I)

np1 = int(trF * len(p1I))
np2 = int(trF * len(p2I))
np3 = int(trF * len(p3I))

rowsTrain = np.hstack((p1I[:np1], p2I[:np2], p3I[:np3]))
Xtrain = X[rowsTrain,:]
Ttrain = T[rowsTrain,:]

rowsTest = np.hstack((p1I[np1:], p2I[np2:], p3I[np3:]))
Xtest = X[rowsTest,:]
Ttest = T[rowsTest,:]

print('Xtrain is {:d} by {:d}. Ttrain is {:d} by {:d}'.format(*(Xtrain.shape + Ttrain.shape)))
uniq = np.unique(Ttrain)
print('   Value  Occurrences')
for i in uniq:
    print('{:7.1g} {:10d}'.format(i, np.sum(Ttrain==i)))

    
print('Xtest is {:d} by {:d}. Ttest is {:d} by {:d}'.format(*(Xtest.shape + Ttest.shape)))
uniq = np.unique(Ttest)
print('   Value  Occurrences')
for i in uniq:
    print('{:7.1g} {:10d}'.format(i, np.sum(Ttest==i)))

names = ['Sepal-length','Sepal-width','Petal-length','Petal-Width','Type']

standardize,_ = makeStandardize(Xtrain)
Xtrain = standardize(Xtrain)
Xtest = standardize(Xtest)

Xtrain1 = addOnes(Xtrain)
Xtest1 = addOnes(Xtest)
w = np.linalg.lstsq( np.dot( Xtrain1.T, Xtrain1 ), np.dot( Xtrain1.T, Ttrain))[0]  #don't forget this [0]

names = ['Sepal-length','Sepal-width','Petal-length','Petal-Width']
names.insert(0,'bias')
i = 0
for i in range(len(names)):
    print('{:2d} {:^20s} {:^10.3g}'.format(i,names[i],w[i][0]))

Ytrain = np.dot( Xtrain1, w)
predictedTrain = np.argmin(np.abs(Ytrain - [0,1,2]),axis=1)
predictedTrain = predictedTrain.reshape((-1,1))
percentCorrectTrain = np.sum(predictedTrain == Ttrain) / float(Ttrain.shape[0]) * 100.0

Ytest = np.dot( Xtest1, w)
predictedTest = np.argmin(np.abs(Ytest - [0,1,2]),axis=1)
predictedTest = predictedTest.reshape((-1,1))
percentCorrectTest = np.sum(predictedTest == Ttest) / float(Ttest.shape[0]) * 100.0

print('Percent Correct: Training {:6.1f} Testing {:6.1f}'.format(percentCorrectTrain, percentCorrectTest))

plt.figure(figsize=(8,6))
plt.plot(np.hstack((Ttrain,predictedTrain)),'o-')
plt.ylim(-0.3,2.3)
plt.xlabel('Sample Index')
plt.ylabel('Class')
plt.title('Training Data')
plt.legend(('Actual','Predicted'),loc='best')
plt.savefig('d502')

plt.figure(figsize=(8,6))
plt.plot(np.hstack((Ttest,predictedTest)),'o-')
plt.ylim(-0.3,2.3)
plt.xlabel('Sample Index')
plt.ylabel('Class')
plt.title('Testing Data')
plt.legend(('Actual','Predicted'),loc='best');
plt.savefig('d503')

plt.figure(figsize=(8,6))
plt.plot(np.hstack((Ttrain,predictedTrain,Ytrain)),'o-')
plt.xlabel('Sample Index')
plt.ylabel('Class')
plt.title('Training Data')
plt.legend(('Actual','Predicted','Model'),loc='best')
plt.savefig('d504')

plt.figure(figsize=(8,6))
plt.plot(np.hstack((Ttest,predictedTest,Ytest)),'o-')
plt.xlabel('Sample Index')
plt.ylabel('Class')
plt.title('Testing Data')
plt.legend(('Actual','Predicted','Model'),loc='best');
plt.savefig('d505')
